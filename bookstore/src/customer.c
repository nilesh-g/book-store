/*
 ============================================================================
 Name        : bookstore.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "customer.h"



void customer_input(cust_t *c)
{
	printf("Enter Customer Name : ");
	scanf("%s",c->c_name);

	printf("Enter Customer address : ");
	scanf("%s",c->address);

	printf("Enter Customer Mobile No : ");
	scanf("%s",c->mobile_no);
}

void show_customer(cust_t *c)
{
	printf("\n-----------------------------------------------\n");
	printf("Name\tAddress\tMobile_No\t\n");
	printf("-----------------------------------------------\n");
	printf("%s\t%s\t%s\t\n",c->c_name,c->address,c->mobile_no);
	printf("\n-----------------------------------------------\n");
}


int main(void)
{
	cust_t c;
	customer_input(&c);
	show_customer(&c);

	return EXIT_SUCCESS;
}
