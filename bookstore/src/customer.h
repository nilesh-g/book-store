/*
 * customer.h
 *
 *  Created on: 21-Jun-2019
 *      Author: mgr
 */

#ifndef CUSTOMER_H_
#define CUSTOMER_H_
typedef struct Customer
{
	char c_name[64];
	char mobile_no[11];
	char address[64];
}cust_t;

void customer_input(cust_t *c);
void show_customer(cust_t *c);




#endif /* CUSTOMER_H_ */
